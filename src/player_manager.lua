local S = core.get_translator("server_manager")


local engine_version_data = {
    ["5.10.0"] = {
        protocol_version = 46,
        formspec_version = 8,
        serialization_version = 29
    },
    ["5.9.1"] = {
        protocol_version = 45,
        formspec_version = 7,
        serialization_version = 29
    },
    ["5.9.0"] = {
        protocol_version = 44,
        formspec_version = 7,
        serialization_version = 29
    },
    ["5.8.1"] = {
        protocol_version = 43,
        formspec_version = 7,
        serialization_version = 29
    },
    ["5.8.0"] = {
        protocol_version = 43,
        formspec_version = 7,
        serialization_version = 29
    },
    ["5.7.0"] = {
        protocol_version = 42,
        formspec_version = 6,
        serialization_version = 29
    },
    ["5.6.1"] = {
        protocol_version = 41,
        formspec_version = 6,
        serialization_version = 29,
    },
    ["5.6.0"] = {
        protocol_version = 41,
        formspec_version = 6,
        serialization_version = 29
    },
    ["5.5.1"] = {
        protocol_version = 40,
        formspec_version = 5,
        serialization_version = 29
    },
    ["5.5.0"] = {
        protocol_version = 40,
        formspec_version = 5,
        serialization_version = 29
    },
    ["5.4.2"] = {
        protocol_version = 39,
        formspec_version = 4,
        serialization_version = 28
    },
    ["5.4.1"] = {
        protocol_version = 39,
        formspec_version = 4,
        serialization_version = 28
    },
    ["5.4.0"] = {
        protocol_version = 39,
        formspec_version = 4,
        serialization_version = 28
    },
    ["5.3.0"] = {
        protocol_version = 39,
        formspec_version = 3,
        serialization_version = 28
    }
}



local function get_engine_data(major, minor, patch)
    local engine_version = ""
    if patch ~= nil then
        engine_version = major .. "." .. minor .. "." .. patch
    elseif major ~= nil then
        engine_version = major
    else
        engine = core.get_version().string
    end

    return engine_version_data[engine_version]
end



local function get_highest_known_protocol_version()
    local highest_protocol_version = -1
    for key, value in pairs(engine_version_data) do
        if value.protocol_version > highest_protocol_version then
            highest_protocol_version = value.protocol_version
        end
    end

    return highest_protocol_version
end



local function get_matching_engine_versions(protocol_version, formspec_version, serialization_version)
    local engine_versions = {}
    for key, value in pairs(engine_version_data) do
        if (not protocol_version or protocol_version == value.protocol_version)
            and (not formspec_version or formspec_version == value.formspec_version)
            and (not serialization_version or serialization_version == value.serialization_version)
            then
            table.insert(engine_versions, key)
        end
    end

    return engine_versions
end




core.register_on_joinplayer(function(player)
    local pl_name = player:get_player_name()
    local info = core.get_player_information(pl_name)

    -- identify flagged cheat clients
    if
        info.version_string
        and (
            string.match(info.version_string, "dragonfire")
            or string.match(info.version_string, "c47eae3") -- dragonfire release 2021.03
            or string.match(info.version_string, "b7abc8d") -- dragonfire release 2021.05
            or string.match(info.version_string, "350b6d1") -- dragonfire release 2022.05
            or string.match(info.version_string, "5.9.0-dev-196a7d3") -- Otter Client (v1.0.1)
            or string.match(info.version_string, "5.9.0-dev-4b62aed") -- Otter Client (v1.0.0)
            or string.match(info.version_string, "garud") -- Garud
            or string.match(info.version_string, "cloakv4") -- Cloakv4
        )
    then
        core.kick_player(pl_name, S("Something is wrong with your client. Try using the official one: https://www.luanti.org/downloads/."))
        core.log("warning", pl_name .. " tried to join with dragonfire, garud or otter client < v1.0.1.")
    end

    -- detect inconsistent engine versions
    if info.major and info.minor and info.patch then
        local engine_data = get_engine_data(info.major, info.minor, info.patch)
        if engine_data ~= nil then
            if
                info.protocol_version ~= engine_data.protocol_version
                or info.formspec_version ~= engine_data.formspec_version
                or (info.serialization_version ~= nil and info.serialization_version ~= engine_data.serialization_version)
            then
                core.kick_player(pl_name, S("Something is wrong with your client. Try using the official one: https://www.luanti.org/downloads/."))
                core.log("warning", pl_name .. "ERROR: tried to join with an unsupported client version (cheating?).")
            end
        end
    else
        -- Fallback version

        -- If their protocol version is equal to the highest we know about, then we shouldn't do the check
        -- Since a newer version may have released with the same protocol version
        -- But we should have all data about previous releases
        -- And we might as well not check newer versions for optimization
        if info.protocol_version < get_highest_known_protocol_version() then
            local engine_versions = get_matching_engine_versions(
                info.protocol_version,
                info.formspec_version,
                info.serialization_version
            )

            if #engine_versions == 0 then
                core.kick_player(pl_name, S("Something is wrong with your client. Try using the official one: https://www.luanti.org/downloads/."))
                core.log("warning", pl_name .. "ERROR: tried to join with an unsupported client version (cheating?).")
            end
        end
    end

    player:set_lighting({
        shadows = {intensity = server_manager.default_shadow_intensity},
        bloom = {intensity = server_manager.default_bloom_intensity}
    })
end)
