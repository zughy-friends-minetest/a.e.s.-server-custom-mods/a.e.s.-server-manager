local S = core.get_translator("server_manager")   -- to translate strings



--== CHAT ==--

server_manager.blocked_cmds = {
	me = true
}

server_manager.blocked_cmds_when_muted = {
	me = true, msg = true, whisper = true, r = true, w = true, tell = true, message = true
}

-- caps prevention
server_manager.min_infringements_number_to_mute_for_caps = 3

-- flood prevention
server_manager.min_infringements_number_to_mute_for_flood = 2
server_manager.min_delay_between_messages = 1.5  -- in seconds,

-- chat filter
server_manager.min_infringements_number_to_mute_for_swearing = 3

-- After the min amount of infringements, every time they'll misbehave,
-- they'll be muted by the next duration in the list (in minutes)
server_manager.punishment_mute_durations = {1, 5, 10, 30}



--== BROADCASTING ==--

server_manager.broadcasts_prefix = "[Server]"
server_manager.broadcast_msgs_color = "#eea160"
server_manager.broadcast_delay = 600  -- in seconds

-- these messages will be displayed in the chat every broadcast_delay seconds
server_manager.broadcast_msgs = {
	S("Got some ideas about how to improve the server? Go to the main square, write us a book and put it into the suggestion box!"),
	S("Why did we choose Luanti instead of Minecraft? Because it's free software!"),
	S("Do you want to support what we do (server and mods)? Offer us a coffee on https://liberapay.com/aes_luanti_server"),
	S("Playing while in a call is way more fun than alone; and we have a Mumble server for that! Join us at mumble.aes.land"),
	S("Looking for people to play with or you want to follow the evolution of the server? Take part in our Matrix community: #arcadeemulationserver:matrix.org (you need a client like Element)"),
	S("Do you like our minigames? Leave a review on https://content.luanti.org!"),
	S("Is the server not translated into your language, or is there some phrase you are not sure about? Contribute to the server translations on Weblate https://translate.codeberg.org/projects/zughy-friends-minetest/ !")
}



--== OTHER ==--

-- not given by default, if you want to change it edit the "default" role
server_manager.build_priv = "build"

server_manager.default_shadow_intensity = 0.2 -- 0.0 to 1.0
server_manager.default_bloom_intensity = 0 -- 0.0 to 1.0

server_manager.time_after_player_gets_afk = 240  -- in seconds

server_manager.max_reports_per_hour = 3  -- per player, so that they can't spam reports

server_manager.users_who_can_bypass_userlimit = {
	"_Zaizen_",
	"Zughy",
	"Giov4",
	"XxCrazyMinerxX",
	"SonoMichele",
	"GabrielOMG"
}

-- custom privs that will be registered when the server starts
server_manager.custom_privs = {
	aes_event = {},

	-- your_priv_name = {
	-- 	description = "something...",
	--		give_to_singleplayer = true/false (true by default),
	--		give_to_admin = true/false (true by default),
	-- }
}
